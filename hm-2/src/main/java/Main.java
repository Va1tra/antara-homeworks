
public class Main {
    private static String[] names;
//    private static String[] carnivoreSpecies;
//    private static String[] herbivoreSpecies;
    private static Class<?>[] carnivoreSpecies;
    private static Class<?>[] herbivoreSpecies;
    private static final int CARNIVORES = 0;
    private static final int HERBIVORES = 1;
    static{
        names = new String[]{
                "Petr", "Akira", "Serafim", "Leila", "Jack", "Uvar", "Bella", "Riko",
                "Viktor", "Saule", "Jerar", "Sonya", "Hian", "Ulusbek", "Varyag", "Putin"
        };
//        herbivoreSpecies = new String[] {"duck", "camel", "lama"};
//        carnivoreSpecies = new String[] {"wolf", "hawk", "panther"};
        herbivoreSpecies = new Class<?>[] {Duck.class, Camel.class, Lama.class};
        carnivoreSpecies = new Class<?>[] {Wolf.class, Hawk.class, Panther.class};
    }
    public static String getName() {
        return names[(int) (Math.random() * names.length)];
    }

    public static Aviary getAviaryWithAnimals(int size, int feed ) {
        Aviary aviary = new Aviary(size);
        Class<?>[] ref = herbivoreSpecies;

        if (feed == CARNIVORES) {
            ref = carnivoreSpecies;
        }

        for (int i = 0; i < size; i++ ) {
            int n = (int)(Math.random() * ref.length);
            aviary.add(getAnimal(ref[n], getName()));
        }

        return  aviary;
    }

    public static Animal getAnimal(Class<?> species, String name) {
        Animal animal = null;
        try {
            animal = (Animal) species.getConstructor(String.class).newInstance(name);
        }catch(Exception e) {
            e.printStackTrace();
        }
        return animal;
//        switch (species) {
//            case "duck" :
//                return new Duck(name);
//            case "camel" :
//                return new Camel(name);
//            case "lama" :
//                return new Lama(name);
//            case "wolf" :
//                return new Wolf(name);
//            case "hawk" :
//                return new Hawk(name);
//            case "panther":
//                return new Panther(name);
//            default:return new Panther(name);
//        }
    }

    public static void main(String[] args) {
        Aviary aviaryWithCarnivores1 = getAviaryWithAnimals(4, CARNIVORES);
        Aviary aviaryWithCarnivores2 = getAviaryWithAnimals(6, CARNIVORES);
        Aviary aviaryWithHerbivores1 = getAviaryWithAnimals(3, HERBIVORES);
        Aviary aviaryWithHerbivores2 = getAviaryWithAnimals(7, HERBIVORES);

//        aviaryWithCarnivores2.feed(new Steak());
//        aviaryWithCarnivores2.feed(new EucalyptusLeaves());
//        aviaryWithCarnivores2.letAnimalsRoar();
         aviaryWithHerbivores2.letAnimalsSpit();
    }
}

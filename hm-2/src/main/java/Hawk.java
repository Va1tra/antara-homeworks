public class Hawk extends Carnivorous implements Flyable{
    public Hawk(String name) {
        super("Hawk", name);
    }
    public void fly() {
        System.out.println(this.getSpecies() + " " + this.getName() + " flies away");
    }
}

public class Aviary {
    public Animal[] animals;
    private int fullness;

    public Aviary(int aviarySize) {
        this.animals = new Animal[aviarySize];
        fullness = 0;
    }

    public void add(Animal animal) {
        if(this.fullness >= this.animals.length) {
            System.out.println("Aviary if full");
            return;
        }
        this.animals[fullness] = animal;
        fullness++;
    }

    public void feed(Food food) {
        for(Animal animal : animals) {
            animal.eat(food);
        }
    }

    public void letAnimalsFly() {
        for(Animal animal : animals) {
            if (!(animal instanceof Flyable)) {
                System.out.println(animal.getSpecies() + " can't fly");
                continue;
            }
            ((Flyable) animal).fly();
        }
    }

    public void letAnimalsSpit() {
        for(Animal animal : animals) {
            if (!(animal instanceof Spitable)) {
                System.out.println(animal.getSpecies() + " can't spit");
                continue;
            }
            ((Spitable) animal).spit();
        }
    }

    public void letAnimalsRoar() {
        for(Animal animal : animals) {
            if (!(animal instanceof Growlable)) {
                System.out.println(animal.getSpecies() + " can't roar");
                continue;
            }
            ((Growlable) animal).roar();
        }
    }
}

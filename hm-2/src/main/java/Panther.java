public class Panther extends Carnivorous implements Growlable{
    public Panther(String name) {
        super("Panther", name);
    }
    public void roar() {
        System.out.println(this.getSpecies() + " " + this.getName() + " roars!");
    }
}

public class Lama extends Herbivore implements Spitable {
    public Lama(String name) {
        super("Lama", name);
    }
    public void spit() {
        System.out.println(this.getSpecies() + " " + this.getName() + " spits right in our face");
    }
}

public abstract class Food {
    private String foodName;
    public Food(String foodName) {
        this.foodName = foodName;
    }
    public String getFoodName() {
        return this.foodName;
    }
}

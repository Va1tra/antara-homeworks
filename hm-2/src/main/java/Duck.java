public class Duck extends Herbivore implements Flyable {
    public Duck(String name) {
        super("Duck", name);
    }
    public void fly() {
        System.out.println(this.getSpecies() + " " + this.getName() + " flies away");
    }
}

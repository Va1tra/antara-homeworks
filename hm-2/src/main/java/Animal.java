public abstract class Animal {
    private String species;
    private String name;
    public Animal(String species, String name) {
        this.species = species;
        this.name = name;
    }
    public String getSpecies() {
        return this.species;
    }
    public String getName() {
        return this.name;
    }
    public abstract void eat(Food food);
}

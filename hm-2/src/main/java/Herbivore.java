public abstract class Herbivore extends Animal {
    public Herbivore(String species, String name) {
        super(species, name);
    }
    public void eat(Food food) {
        if (food instanceof MeatFood) {
            System.out.println(this.getSpecies() + " doesn't eat " + food.getFoodName());
        } else {
            System.out.println(this.getSpecies() + " " + this.getName() + " ate " + food.getFoodName());
        }
    }
}

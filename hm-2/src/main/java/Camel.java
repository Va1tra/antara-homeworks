public class Camel extends Herbivore implements Spitable {
    public Camel(String name) {
        super("Camel", name);
    }
    public void spit() {
        System.out.println(this.getSpecies() + " " + this.getName() + " spits right in our face");
    }
}

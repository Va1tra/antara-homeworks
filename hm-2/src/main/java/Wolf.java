public class Wolf extends Carnivorous implements Growlable{
    public Wolf(String name) {
        super("Wolf", name);
    }
    public void roar() {
        System.out.println(this.getSpecies() + " " + this.getName() + " roars!");
    }
}

import somepackage.Other;
import somepackage.Some;

public class Main {
    static void integralTypes() {
        byte b = 116;
        short s = 1123;
        int i = 64536;
        long l = 2147483648L; // Постфикс l или L обозначает литералы типа long
        System.out.println(i);
        System.out.println(b);
        System.out.println(s);
        System.out.println(l);
    }

    static void characters() {
        char a = 'a', b, c = 'c';
        b = (char) ((a + c) / 2); // Можно складывать, вычитать, делить и умножать
        // Но из-за особенностей арифметики Java результат приходится приводить к типу char явно
        System.out.println(b); // Выведет символ 'b'
    }

    static void floatingPointTypes() {
        double a, b = 4.12;
        a = 22.1 + b;
        float pi = 3.14f; // При использовании типа float требуется указывать суффикс f или F
        // так как без них типом литерала будет считаться double
        float anotherPi = (float) 3.14; // Можно привести явно
        double c = 27;
        double d = pi * c;
        System.out.println(d);
    }

    static void strings() {
        String a = "Hello", b = "World";
        System.out.println(a + " " + b); // Здесь + означает объединение (конкатенацию) строк
        // Пробел не вставляется автоматически

        // Строки конкатенируются слева направо, надо помнить это когда соединяешь строку и примитив
        String c = 2 + 2 + ""; // "4"
        String d = "" + 2 + 2; // "22"
        d = "" + (2 + 2); // а теперь d тоже "4"

        String foo = "a string";
        String bar = "a string"; // bar будет указывать на тот же объект что и foo
        String baz = new String("a string"); // Чтобы гарантированно создать новую строку надо вызвать конструктор
        System.out.println("foo == bar ? " + (foo == bar)); // == сравнивает ссылки на объекты
        System.out.println("foo равен bar ? " + (foo.equals(bar))); // Метод equals служит для проверки двух объектов на равенство
        System.out.println("foo == baz ? " + (foo == baz));
        System.out.println("foo равен baz ? " + (foo.equals(baz)));

        String str1 = "Java";
        String str2 = "Hello";
        String str12 = str2.concat(str1); // HelloJava

        String str3 = String.join(" ", str2, str1); // Hello Java

        char z = str1.charAt(2);
        System.out.println(z); // v
    }

    static void wrappers() {
        int i = 1;
        Integer boxed;
        //boxed = new Integer(i); // Обычное создание объекта
        boxed = Integer.valueOf(i); // Фабричный метод
        System.out.println(boxed);
        boxed = i; // Автоматическая упаковка, компилятор просто вставит вызов Integer.valueOf
        System.out.println(boxed);

        Integer unboxed = Integer.valueOf(5);
        int j;
        j = unboxed.intValue(); // Явная распаковка
        System.out.println(j);
        j = unboxed; // Автоматическая распаковка
        System.out.println(j);
    }

    public static void mostFrequentlyLetterInWord(String word) {
        char[] wordChars = word.toLowerCase().toCharArray();
        int[] numberOfLetters = new int[26];
        int i = wordChars.length - 1;

        while(i >= 0) {
            if(wordChars[i] > 96 && wordChars[i] < 123) {
                numberOfLetters[wordChars[i] - 97]++;
            }
            i--;
        }

        for (int j = 0; j < numberOfLetters.length; j++) {
            System.out.println( "letter "+(char)(j + 97) + " met " + numberOfLetters[j] + " times");
        }
    }

    public static void sumOfDigitsInArray(int[] array) {
        int sum = 0;

        for(int i : array) {
            sum += sumOfDigits(i);
        }
        System.out.println(sum);
    }

    public static int sumOfDigits(int n) {
        if (n < 0) n = Math.abs(n);
        int sum = 0;
        int digit;

        while(n > 0) {
            digit = n % 10;
            sum += digit;
            n /= 10;
        }

        return sum;
    }

    public static void multiplication(int[] array) {
        int mult = 1;
        int i = 0;
        do{
           mult *= array[i];
           i++;
        } while(i < array.length);
    }

    public static void main(String[] args) {
        sumOfDigitsInArray(new int[]{123, 456, 789});
        mostFrequentlyLetterInWord("Supercalifragilisticexpialidocious");
        multiplication(new int[] {1, 3, 6, 8, 12, 44, 9, 54, -1});
        //integralTypes();
        //characters();
        //floatingPointTypes();
        //strings();
        //wrappers();

    }
}
